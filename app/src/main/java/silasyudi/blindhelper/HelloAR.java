package silasyudi.blindhelper;

import java.util.ArrayList;

import android.opengl.GLES20;

import silasyudi.blindhelper.access.Access;

import cn.easyar.*;

/**
 * Classe que faz a busca pelas imagens.
 */
public class HelloAR {

    private CameraDevice camera;
    private CameraFrameStreamer streamer;
    private ArrayList<ImageTracker> trackers;
    private Renderer videobg_renderer;
    private boolean viewport_changed = false;
    private Vec2I view_size = new Vec2I(0, 0);
    private int rotation = 0;
    private Vec4I viewport = new Vec4I(0, 0, 1280, 720);

    private HandleAccessibility accessibility;
    private Mark mark;
    private Access talk;

    /**
     * Uma handle interface para executar acoes de acessibilidade entre a classe HelloAR e GLView.
     */
    public interface HandleAccessibility {

        /**
         * Executa uma acao de acessibilidade.
         *
         * @param info a informacao a ser executada
         */
        void access(String info);
    }

    public HelloAR(Access talk) {
        trackers = new ArrayList<>();
        this.talk = talk;
    }

    /**
     * Recupera as imagens a serem buscadas pelo GLView.
     *
     * @param tracker o objeto tracker
     * @param path    o caminho do JSON
     */
    private void loadAllFromJsonFile(ImageTracker tracker, String path) {
        for (ImageTarget target : ImageTarget.setupAll(path, StorageType.Assets)) {
            Mark.add(target.name());
            tracker.loadTarget(target, new FunctorOfVoidFromPointerOfTargetAndBool() {
                @Override
                public void invoke(Target target, boolean status) {
                    Logger.info(
                            "load target (" + status + "): "
                                    + target.name()
                                    + " (" + target.runtimeID() + ")"
                    );
                }
            });
        }
    }

    /**
     * Inicializa a deteccao.
     *
     * @param accessibility um handle para executar acoes de acessibilidade ao detectar imagens
     * @return <b>TRUE</b>, caso inicialize corretamente
     */
    public boolean initialize(HandleAccessibility accessibility) {
        camera = new CameraDevice();
        streamer = new CameraFrameStreamer();
        streamer.attachCamera(camera);
        this.accessibility = accessibility;

        boolean status = true;
        status &= camera.open(CameraDeviceType.Default);
        camera.setSize(new Vec2I(1280, 720));

        if (status) {
            ImageTracker tracker = new ImageTracker();
            tracker.attachStreamer(streamer);
            loadAllFromJsonFile(tracker, MainActivity.getProperties().getProperty("marcadores"));
            trackers.add(tracker);

            return true;
        }

        return false;
    }

    /**
     * Encerra a deteccao.
     */
    public void dispose() {
        for (ImageTracker tracker : trackers) {
            tracker.dispose();
        }
        trackers.clear();
        if (videobg_renderer != null) {
            videobg_renderer.dispose();
            videobg_renderer = null;
        }
        if (streamer != null) {
            streamer.dispose();
            streamer = null;
        }
        if (camera != null) {
            camera.dispose();
            camera = null;
        }
    }

    /**
     * Inicializa a deteccao.
     *
     * @return <b>TRUE</b>, caso inicialize todos os componentes corretamente
     */
    public boolean start() {
        boolean status = true;
        status &= (camera != null) && camera.start();
        status &= (streamer != null) && streamer.start();
        camera.setFocusMode(CameraDeviceFocusMode.Continousauto);
        for (ImageTracker tracker : trackers) {
            status &= tracker.start();
        }
        return status;
    }

    /**
     * Paralisa a deteccao.
     *
     * @return <b>TRUE</b>, caso paralise todos os componentes corretamente
     */
    public boolean stop() {
        boolean status = true;
        for (ImageTracker tracker : trackers) {
            status &= tracker.stop();
        }
        status &= (streamer != null) && streamer.stop();
        status &= (camera != null) && camera.stop();
        return status;
    }

    /**
     * Inicializa a renderizacao de video.
     */
    public void initGL() {
        if (videobg_renderer != null) {
            videobg_renderer.dispose();
        }
        videobg_renderer = new Renderer();
    }

    /**
     * Redimensiona a renderizacao de video.
     */
    public void resizeGL(int width, int height) {
        view_size = new Vec2I(width, height);
        viewport_changed = true;
    }

    /**
     * Modifica a angulacao do viewport.
     */
    private void updateViewport() {
        CameraCalibration calib = camera != null ? camera.cameraCalibration() : null;
        int rotation = calib != null ? calib.rotation() : 0;
        if (rotation != this.rotation) {
            this.rotation = rotation;
            viewport_changed = true;
        }
        if (viewport_changed) {
            Vec2I size = new Vec2I(1, 1);
            if ((camera != null) && camera.isOpened()) {
                size = camera.size();
            }
            if (rotation == 90 || rotation == 270) {
                size = new Vec2I(size.data[1], size.data[0]);
            }
            float scaleRatio = Math.max(
                    (float) view_size.data[0] / (float) size.data[0],
                    (float) view_size.data[1] / (float) size.data[1]
            );
            Vec2I viewport_size = new Vec2I(
                    Math.round(size.data[0] * scaleRatio),
                    Math.round(size.data[1] * scaleRatio)
            );
            viewport = new Vec4I(
                    (view_size.data[0] - viewport_size.data[0]) / 2,
                    (view_size.data[1] - viewport_size.data[1]) / 2,
                    viewport_size.data[0], viewport_size.data[1]
            );

            if ((camera != null) && camera.isOpened())
                viewport_changed = false;
        }
    }

    /**
     * Renderiza o video.
     */
    public void render() {
        GLES20.glClearColor(1.f, 1.f, 1.f, 1.f);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);

        if (videobg_renderer != null) {
            Vec4I default_viewport = new Vec4I(0, 0, view_size.data[0], view_size.data[1]);
            GLES20.glViewport(
                    default_viewport.data[0],
                    default_viewport.data[1],
                    default_viewport.data[2],
                    default_viewport.data[3]
            );
            if (videobg_renderer.renderErrorMessage(default_viewport)) {
                return;
            }
        }

        if (streamer == null) {
            return;
        }
        Frame frame = streamer.peek();
        try {
            updateViewport();
            GLES20.glViewport(
                    viewport.data[0],
                    viewport.data[1],
                    viewport.data[2],
                    viewport.data[3]
            );

            if (videobg_renderer != null) {
                videobg_renderer.render(frame, viewport);
            }

            for (TargetInstance targetInstance : frame.targetInstances()) {
                int status = targetInstance.status();
                if (status == TargetStatus.Tracked) {
                    Target target = targetInstance.target();
                    ImageTarget imagetarget = target instanceof ImageTarget ? (ImageTarget) (target) : null;
                    if (imagetarget == null) {
                        continue;
                    }

                    setMarkTarget(targetInstance.target().name());
                    accessibility.access(
                            MainActivity.getProperties()
                                    .getProperty("marcadores_handle")
                                    + " " + mark.getNome()
                    );
                }
            }
        } finally {
            frame.dispose();
        }
    }

    /**
     * Seta a marcacao detectada.
     *
     * @param nome o nome da marcacao
     */
    public void setMarkTarget(String nome) {
        try {
            mark = Mark.getInstance(nome);
        } catch (IllegalArgumentException e) {
            Logger.erro(e);
            mark = null;
        }
    }

    /**
     * Recupera a marcacao detectada mais recentemente.
     *
     * @return a marcacao detectada mais recentemente
     */
    public Mark getMarkTarget() {
        return mark;
    }

    /**
     * Limpa a marcacao detectada.
     */
    public void clearMarkTarget() {
        mark = null;
    }
}
