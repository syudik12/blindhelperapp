package silasyudi.blindhelper;

import java.util.ArrayList;
import java.util.List;

public final class Mark {

    public final static String MARK_INTENT = "mark";

    private static List<Mark> marks;

    private String nome;

    static {
        marks = new ArrayList<>();
    }

    private Mark(String nome) {
        this.nome = nome;
    }

    public int getId() {
        return marks.indexOf(this);
    }

    public String getNome() {
        return this.nome;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Mark) {
            return this.nome.equals(((Mark) o).getNome());
        }

        return false;
    }

    public static void add(String nome) {
        Mark m = new Mark(nome);
        if (!marks.contains(m)) {
            marks.add(m);
        }
    }

    public static Mark getInstance(String nome) {
        Mark m = new Mark(nome);
        return (marks.contains(m)) ? m : null;
    }
}