package silasyudi.blindhelper;

import android.util.Log;

public class Logger {

    private static final String TAG = "COM.SILAS";

    public static void erro(Exception e) {
        Log.e(TAG, e.getMessage());
    }

    public static void erro(String msg) {
        Log.e(TAG, msg);
    }

    public static void debug(String msg) {
        Log.d(TAG, msg);
    }

    public static void info(String msg) {
        Log.i(TAG, msg);
    }

}
