package silasyudi.blindhelper;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Classe que recebe e faz o parse dos dados do JSON.
 *
 * @author Silas Yudi {syudik12@gmail.com}
 */
public class Database {

    private JSONArray global;
    private JSONArray mark;
    private Stack<JSONObject> pilha;

    /**
     * @param context o contexto
     * @param file    o arquivo JSON
     * @param mark    a marcacao selecionada
     */
    public Database(Context context, String file, Mark mark) {
        this.global = JSONParser.getParserToArray(context, file);
        this.mark = selectMark(this.global, mark);
        this.pilha = new Stack<>();
    }

    /**
     * Verifica se o campo selecionado eh um outro objeto.
     *
     * @param campo o campo de busca
     * @return TRUE se for objeto, FALSE se a pilha estiver vazia ou se nao for objeto
     */
    public boolean isJSONObject(String campo) {
        try {
            Object o = pilha.peek().get(campo);

            if (o instanceof JSONObject) {
                return true;
            }
        } catch (JSONException e) {
            Logger.erro(e);
        }

        return false;
    }

    /**
     * Recupera uma informacao de um menu.
     *
     * @param campo o campo de busca
     * @return a informacao
     */
    public String getInfoFromObject(String campo) {
        try {
            return pilha.peek().getString(campo);
        } catch (JSONException e) {
            Logger.erro(e);
        }

        return "";
    }

    /**
     * Recupera os campos de informacao de um menu.
     *
     * @param campo o identificador do menu
     * @return a lista de campos de informacao de um menu
     */
    public List<String> getJSONFromObject(String campo) {
        try {
            JSONObject selecionado = pilha.peek().getJSONObject(campo);
            pilha.push(selecionado);

            return populateList(selecionado.names());
        } catch (JSONException e) {
            Logger.erro(e);
        }

        return new ArrayList<>();
    }

    /**
     * Recupera os campos de informacao de um menu.
     *
     * @param selecionado o identificador do menu
     * @return a lista de campos de informacao de um menu
     */
    public List<String> getJSONFromMark(int selecionado) {
        try {
            JSONObject select = mark.getJSONObject(selecionado);
            JSONArray properties = select.names();
            pilha.push(select);

            return populateList(properties);
        } catch (JSONException e) {
            Logger.erro(e);
        }

        return new ArrayList<>();
    }

    /**
     * Recupera as informacoes da marcacao desta database para popular o menu inicial.
     *
     * @return as informacoes da marcacao desta database
     * @throws JSONException
     */
    public List<String> populateMenu() {
        List<String> lista = new ArrayList<>();

        try {
            for (int i = 0; i < mark.length(); i++) {
                lista.add(mark.getJSONObject(i).getString(
                        MainActivity.getProperties().getProperty("legenda")
                ));
            }
        } catch (JSONException e) {
            Logger.erro(e);
        }

        return lista;
    }

    public boolean pilhaIsEmpty() {
        return pilha.isEmpty();
    }

    /**
     * Retorna a um menu anterior.
     *
     * @return
     */
    public List<String> returnMenu() {
        pilha.pop();

        if (pilha.isEmpty()) {
            return populateMenu();
        }

        JSONObject retornado = pilha.peek();

        return populateList(retornado.names());
    }

    /**
     * Popula a lista a ser retornado 'a interface.
     *
     * @param properties o array com as propriedades a serem populadas
     * @return
     */
    private List<String> populateList(JSONArray properties) {
        List<String> lista = new ArrayList<>();
        String legenda = MainActivity.getProperties().getProperty("legenda");

        try {
            for (int i = 0; i < properties.length(); i++) {
                String p = properties.getString(i);

                if (p.equals(legenda)) {
                    continue;
                }

                lista.add(properties.getString(i));
            }
        } catch (JSONException e) {
            Logger.erro(e);
        }

        return lista;
    }

    /**
     * Seleciona uma marcacao cujas as informacoes serao populadas na variavel <i>base</i>.
     *
     * @param global o objeto JSON contendo todas as marcacoes
     * @param mark   a marcacao a ser extraida as informacoes
     */
    private JSONArray selectMark(JSONArray global, Mark mark) {
        try {
            return global.getJSONArray(mark.getId());
        } catch (JSONException e) {
            Logger.erro(e);
        }

        return new JSONArray();
    }
}
