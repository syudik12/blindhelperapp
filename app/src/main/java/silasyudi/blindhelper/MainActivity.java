package silasyudi.blindhelper;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.WindowManager;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Properties;

import cn.easyar.Engine;

public class MainActivity extends GravitySensor {

    private static String key = "BGNLRbq99u98Al99eNZCT334R2KBXeGhOnoaah8d3dJ1ThGWkOz2DmXNpVlEWmC8M7OoVzCE52DwGr7gAZfaQC5JQxqe4zxWuOVWerqVpeWE00ofJmy4yfHYGXbSqTYAVeRGmB0GOuu1BpFhg0suYszjlYXoldN7WQjwg99BNxak2oZqq3Mwq39DJhFqwqcHE1KJJKf6";
    private static Properties properties;
    private GLView glView;
    private HashMap<Integer, PermissionCallback> permissionCallbacks = new HashMap<Integer, PermissionCallback>();
    private int permissionRequestCodeSerial = 0;

    private interface PermissionCallback {
        void onSuccess();

        void onFailure();
    }

    public static Properties getProperties() {
        return properties;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setConfigs();

        if (!Engine.initialize(this, key)) {
            Logger.erro("Initialization Failed.");
        }

        glView = new GLView(this);

        requestCameraPermission(new PermissionCallback() {
            @Override
            public void onSuccess() {
                ((ViewGroup) findViewById(R.id.preview)).addView(
                        glView,
                        new ViewGroup.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.MATCH_PARENT
                        )
                );
            }

            @Override
            public void onFailure() {
            }
        });


    }

    @TargetApi(23)
    private void requestCameraPermission(PermissionCallback callback) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                int requestCode = permissionRequestCodeSerial;
                permissionRequestCodeSerial += 1;
                permissionCallbacks.put(requestCode, callback);
                requestPermissions(new String[]{Manifest.permission.CAMERA}, requestCode);
            } else {
                callback.onSuccess();
            }
        } else {
            callback.onSuccess();
        }
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode,
            @NonNull String[] permissions,
            @NonNull int[] grantResults
    ) {
        if (permissionCallbacks.containsKey(requestCode)) {
            PermissionCallback callback = permissionCallbacks.get(requestCode);
            permissionCallbacks.remove(requestCode);
            boolean executed = false;
            for (int result : grantResults) {
                if (result != PackageManager.PERMISSION_GRANTED) {
                    executed = true;
                    callback.onFailure();
                }
            }
            if (!executed) {
                callback.onSuccess();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onStart() {
        super.onStart();
        changeStateGlView(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        changeStateGlView(true);
    }

    @Override
    protected void onPause() {
        changeStateGlView(false);
        super.onPause();
    }

    @Override
    protected void onStop() {
        changeStateGlView(false);
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void actionListener(Action action) {
        if (action == Action.NEXT && (glView.getMarkSelected() != null)) {
            destroiSensor();
            Intent intent = new Intent(MainActivity.this, NavigationActivity.class);
            intent.putExtra(Mark.MARK_INTENT, glView.getMarkSelected().getNome());
            glView.clearMarkSelected();
            startActivity(intent);
        }
    }

    private void setConfigs() {
        properties = new Properties();

        try {
            properties.load(new InputStreamReader(
                    getAssets().open("config.properties"),
                    "UTF-8"
            ));
            Logger.info(properties.toString());
        } catch (IOException e) {
            Logger.erro(e);
        }
    }

    /**
     * Muda o status de execucao da renderizacao de video e busca de imagens.
     *
     * @param setResume <b>TRUE</b>, para reiniciar, <b>FALSE</b>, para paralisar
     */
    private void changeStateGlView(boolean setResume) {
        if (glView == null) {
            return;
        }

        if (setResume) {
            glView.onResume();
        } else {
            glView.onPause();
        }
    }
}
