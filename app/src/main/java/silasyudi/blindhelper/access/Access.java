package silasyudi.blindhelper.access;

import android.content.Context;

/**
 * Uma forma de prover acesso a informacao.
 *
 * @author Silas Yudi {syudik12@gmail.com}
 */
public abstract class Access {

    protected Context context;
    protected String info;

    /**
     * @param context o contexto
     */
    public Access(Context context) {
        this(context, "");
    }

    /**
     * @param context o contexto
     * @param info    a informacao a ser acessada
     */
    public Access(Context context, String info) {
        this.context = context;
        this.info = info;
    }

    /**
     * Altera a informacao desta acao de acessibilidade.
     *
     * @param info a nova informacao
     */
    public void setInfo(String info) {
        this.info = info;
    }

    /**
     * Retorna o contexto da execucao das acoes.
     *
     * @return o contexto da execucao das acoes
     */
    public Context getContext() {
        return this.context;
    }

    /**
     * Executa uma acao de acessibilidade.
     */
    public abstract void giveAccess();

    /**
     * Verifica se a acao de acessibilidade ainda esta sendo executada.
     *
     * @return <b>TRUE</b> se esta sendo executada uma acao de acessibilidade,
     * <b>FALSE</b>, do contrario
     */
    public abstract boolean isGivingAccess();

}
