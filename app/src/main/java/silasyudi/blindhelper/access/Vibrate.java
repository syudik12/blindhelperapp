package silasyudi.blindhelper.access;

import android.content.Context;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;

import silasyudi.blindhelper.Logger;

import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import silasyudi.blindhelper.Logger;

/**
 * Classe que executa a vibracao nas acoes da aplicacao.
 *
 * @author Silas Yudi {syudik12@gmail.com}
 */
public class Vibrate extends Access {

    private Vibrator v;
    private boolean isVibrating;
    private ScheduledThreadPoolExecutor notificador;

    private Runnable notificadorRunnable = new Runnable() {
        @Override
        public void run() {
            setVibrating(false);
        }
    };

    /**
     * @param context o contexto
     */
    public Vibrate(Context context) {
        this(context, "");
    }

    /**
     * @param context o contexto
     * @param info    a informacao a ser acessada
     */
    public Vibrate(Context context, String info) {
        super(context, info);

        v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        setVibrating(false);
        notificador = new ScheduledThreadPoolExecutor(1);
    }

    @Override
    public void giveAccess() {
        setVibrating(true);

        Level level;

        try {
            level = Level.valueOf(info);
        } catch (IllegalArgumentException ex) {
            setVibrating(false);
            return;
        }

        if (level == Vibrate.Level.NO_VIBRATION) {
            setVibrating(false);
            return;
        }

        if (Build.VERSION.SDK_INT >= 26) {
            v.vibrate(VibrationEffect.createOneShot(level.getMilis(), VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            v.vibrate(level.getMilis());
        }

        notifyOnVibrationEnd(level.getMilis());
    }

    @Override
    public boolean isGivingAccess() {
        return isVibrating;
    }

    /**
     * Muda o status deste objeto vibrate.
     *
     * @param isVibrating <b>TRUE</b>, caso esteja vibrando, <b>FALSE</b>, caso nao esteja
     */
    private void setVibrating(boolean isVibrating) {
        this.isVibrating = isVibrating;
    }

    /**
     * Notifica que a vibracao encerrou.
     *
     * @param milliseconds o tempo de vibracao
     */
    private void notifyOnVibrationEnd(long milliseconds) {
        try {
            notificador.schedule(notificadorRunnable, milliseconds, TimeUnit.MILLISECONDS);
        } catch (RejectedExecutionException e) {
            Logger.erro(e.getMessage());
        }
    }

    /**
     * Niveis de vibracao.
     */
    public enum Level {
        NO_VIBRATION(0),
        FAST(200),
        DEFAULT(500),
        SLOW(1000);

        private final int milis;

        Level(int m) {
            milis = m;
        }

        public int getMilis() {
            return milis;
        }
    }
}
