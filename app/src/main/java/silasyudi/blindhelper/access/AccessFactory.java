package silasyudi.blindhelper.access;

import android.content.Context;

/**
 * Classe para fabricar os itens de feedback de acessibilidade.
 *
 * @author Silas Yudi {syudik12@gmail.com}
 */
public final class AccessFactory {

    /**
     * @param context o contexto
     * @param type    o tipo de acessibilidade
     * @return o tipo de acessibilidade
     */
    public static Access getAccess(Context context, AccessType type) {
        return getAccess(context, type, "");
    }

    /**
     * @param context o contexto
     * @param type    o tipo de acessibilidade
     * @param info    a informacao
     * @return o tipo de acessibilidade
     */
    public static Access getAccess(Context context, AccessType type, String info) {
        switch (type) {
            case VIBRATE:
                return new Vibrate(context, info);
            default:
            case TALK:
                return new Talk(context, info);
        }
    }
}
