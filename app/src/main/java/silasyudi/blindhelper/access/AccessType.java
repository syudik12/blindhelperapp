package silasyudi.blindhelper.access;

/**
 * Tipos disponiveis de acessibilidade.
 *
 * @author Silas Yudi {syudik12@gmail.com}
 */
public enum AccessType {
    TALK,
    VIBRATE;
}