package silasyudi.blindhelper.access;

import android.content.Context;
import android.speech.tts.TextToSpeech;

import java.util.Locale;

/**
 * Classe que executa as leituras dos textos da aplicacao.
 *
 * @author Silas Yudi {syudik12@gmail.com}
 */
public class Talk extends Access {

    private TextToSpeech tts;
    private Locale locale;
    public static final Locale PT_BR = new Locale("pt", "br");

    /**
     * @param context o contexto
     */
    public Talk(Context context) {
        this(context, "", PT_BR);
    }

    /**
     * @param context o contexto
     * @param info    a informacao a ser acessada
     */
    public Talk(Context context, String info) {
        this(context, info, PT_BR);
    }

    /**
     * @param context o contexto
     * @param info    a informacao a ser acessada
     * @param locale  o idioma a ser utilizado nas falas
     */
    public Talk(Context context, String info, Locale locale) {
        super(context, info);

        this.locale = locale;
    }

    @Override
    public void giveAccess() {
        if (info != null && !info.isEmpty()) {
            tts = new TextToSpeech(context, new TextToSpeech.OnInitListener() {
                @Override
                public void onInit(int status) {
                    if (status == TextToSpeech.SUCCESS) {
                        tts.setLanguage(locale);
                        tts.speak(info, TextToSpeech.QUEUE_FLUSH, null);
                    }
                }
            });
        }
    }

    @Override
    public boolean isGivingAccess() {
        if (tts == null) {
            return false;
        }

        return tts.isSpeaking();
    }
}
