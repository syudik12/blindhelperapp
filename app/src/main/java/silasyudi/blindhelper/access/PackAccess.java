package silasyudi.blindhelper.access;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * Encapsula varios acessos.
 *
 * @author Silas Yudi {syudik12@gmail.com}
 */
public class PackAccess {

    private Context context;
    private List<Access> lista;

    /**
     * @param context o contexto
     */
    public PackAccess(Context context) {
        this.context = context;
        lista = new ArrayList<>();
    }

    /**
     * Adiciona um tipo qualquer de acesso.
     *
     * @param type o tipo de acesso
     * @param info a informacao do acesso
     */
    public PackAccess addAccess(AccessType type, String info) {
        lista.add(AccessFactory.getAccess(context, type, info));

        return this;
    }

    /**
     * Adiciona um acesso do tipo Talk.
     *
     * @param talk a informacao a ser falada
     * @return o objeto PackAccess
     */
    public PackAccess addTalkAccess(String talk) {
        return this.addTalkAndVibrateAccess(talk, Vibrate.Level.NO_VIBRATION);
    }

    /**
     * Adiciona um acesso do tipo Vibrate.
     *
     * @param vibrate o nivel da vibracao
     * @return o objeto PackAccess
     */
    public PackAccess addVibrateAccess(Vibrate.Level vibrate) {
        return this.addTalkAndVibrateAccess("", vibrate);
    }

    /**
     * Adiciona um acesso do tipo Talk e do tipo Vibrate.
     *
     * @param talk    a informacao do tipo Talk
     * @param vibrate o nivel de vibracao
     * @return o objeto PackAccess
     */
    public PackAccess addTalkAndVibrateAccess(String talk, Vibrate.Level vibrate) {
        if (!talk.isEmpty()) {
            this.addAccess(AccessType.TALK, talk);
        }

        if (vibrate != Vibrate.Level.NO_VIBRATION) {
            this.addAccess(AccessType.VIBRATE, vibrate.name());
        }

        return this;
    }

    /**
     * Executa todos os acessos deste pack.
     */
    public void giveAccess() {
        for (Access a : lista) {
            if (!a.isGivingAccess()) {
                a.giveAccess();
            }
        }
    }

    public boolean isGivingAccess () {
        for (Access a : lista) {
            if (a.isGivingAccess()) {
                return true;
            }
        }

        return false;
    }

}
