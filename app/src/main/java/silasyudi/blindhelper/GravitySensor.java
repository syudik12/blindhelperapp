package silasyudi.blindhelper;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Classe que administra o sensor acelerometro.
 *
 * @author Silas Yudi {syudik12@gmail.com}
 */
public abstract class GravitySensor extends AppCompatActivity implements SensorEventListener {

    // Sensores
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;

    // Despreza o ruido
    protected static final float NOISE = 4f;

    // Actions
    public enum Action {
        NEXT, PREVIEW, UP, DOWN, NO_ACTION;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }

    @Override
    protected void onStart() {
        super.onStart();
        changeStateSensor(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        changeStateSensor(true);
    }

    @Override
    protected void onPause() {
        changeStateSensor(false);
        super.onPause();
    }

    @Override
    protected void onStop() {
        changeStateSensor(false);
        super.onStop();
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // ignored
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        changeStateSensor(false);

        float x = event.values[0];
        float y = event.values[1];

        Action action = getAction(x, y, NOISE);

        if (action != Action.NO_ACTION) {
            actionListener(action);
        }

        changeStateSensor(true);
    }

    /**
     * Muda o status de execucao do sensor.
     *
     * @param setResume <b>TRUE</b> para reiniciar, <b>FALSE</b> para paralisar o sensor
     */
    protected synchronized void changeStateSensor(boolean setResume) {
        if (setResume) {
            if (mAccelerometer == null) {
                mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            }

            mSensorManager.registerListener(
                    this,
                    mAccelerometer,
                    SensorManager.SENSOR_DELAY_NORMAL
            );
        } else {
            mSensorManager.unregisterListener(this);
        }
    }

    /**
     * Destroi o sensor.
     */
    protected void destroiSensor() {
        changeStateSensor(false);
        this.mAccelerometer = null;
    }

    /**
     * Recupera uma acao.
     *
     * @param x     a coordenada X
     * @param y     a coordenada Y
     * @param noise o ruido a ser ignorado
     * @return a acao a ser executada
     */
    protected static Action getAction(float x, float y, float noise) {
        if (Math.abs(x) < noise) x = 0;
        if (Math.abs(y) < noise) y = 0;

        if (x != 0 || y != 0) {
            if (Math.abs(x) > Math.abs(y)) {
                return x > 0 ? Action.PREVIEW : Action.NEXT;
            }

            return y > 0 ? Action.UP : Action.DOWN;
        }

        return Action.NO_ACTION;
    }

    /**
     * Executa uma action.
     *
     * @param action a acao que foi ouvida
     */
    protected abstract void actionListener(Action action);

}