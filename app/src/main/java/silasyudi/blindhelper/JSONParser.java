package silasyudi.blindhelper;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public final class JSONParser {

    public static JSONArray getParserToArray(Context context, String file) {
        JSONArray json = new JSONArray();

        try {
            json = new JSONArray(getFromFile(context, file));
        } catch (JSONException e) {
            Logger.erro(e);
        }

        return json;
    }

    public static JSONObject getParserToObject(Context context, String file) {
        JSONObject json = new JSONObject();

        try {
            json = new JSONObject(getFromFile(context, file));
        } catch (JSONException e) {
            Logger.erro(e);
        }

        return json;
    }

    private static String getFromFile(Context context, String file) {
        try {
            InputStream inputStream = context.getAssets().open(file);
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(inputStream, "UTF-8")
            );
            StringBuilder stringBuilder = new StringBuilder();

            String line = "";
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }

            bufferedReader.close();
            inputStream.close();

            return stringBuilder.toString();
        } catch (IOException e) {
            Logger.erro(e);
            return "";
        }
    }
}
