package silasyudi.blindhelper;

import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

import silasyudi.blindhelper.access.PackAccess;
import silasyudi.blindhelper.access.Vibrate;

/**
 * Activity de navegacao na arvore de itens de menu. Implementa a interface que recebe os eventos
 * do acelerometro.
 *
 * @author Silas Yudi {syudik12@gmail.com}
 */
public class NavigationActivity extends GravitySensor {

    // A base de dados das obras
    private static Database data;
    // A lista atualmente em tela
    private List<String> lista;
    // A marcacao selecionada
    private Mark mark;
    // O menu atualmente selecionado em tela
    private int selectedMenu;
    // Se ja se iniciou totalmente a navegacao
    private boolean started = false;
    // Se pre-selecionou o retorno ao MainActivity
    private boolean isPreFinish;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);

        Intent intent = getIntent();
        mark = Mark.getInstance(intent.getStringExtra(Mark.MARK_INTENT));

        isPreFinish = false;
        selectedMenu = -1;

        data = new Database(this, MainActivity.getProperties().getProperty("dados"), mark);
        lista = data.populateMenu();

        reloadView(lista);

        started = true;

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    protected void actionListener(Action action) {
        if (started) {
            switch (action) {
                case PREVIEW:
                    trataActionPreview();
                    break;
                case NEXT:
                    trataActionNext();
                    break;
                case UP:
                    trataActionUp();
                    break;
                case DOWN:
                    trataActionDown();
                    break;
            }
        }
    }

    private void waitAccessEnd(PackAccess a) {
        while (a.isGivingAccess()) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                Logger.erro(e);
            }
        }
    }

    /**
     * Metodo para tratar de uma acao de retorno.
     */
    private void trataActionPreview() {
        PackAccess packAccess = new PackAccess(this);

        selectedMenu = -1;

        if (isPreFinish) {
            packAccess
                    .addTalkAndVibrateAccess(
                            MainActivity.getProperties().getProperty("prefinish_preview"),
                            Vibrate.Level.SLOW)
                    .giveAccess();
            finish();
            return;
        }

        String frase = "";

        if (data.pilhaIsEmpty()) {
            isPreFinish = true;
            frase = "mark_preview";
        } else {
            lista = data.returnMenu();
            reloadView(lista);
            frase = "info_preview";
        }

        packAccess.addTalkAndVibrateAccess(
                MainActivity.getProperties().getProperty(frase),
                Vibrate.Level.DEFAULT
        );

        packAccess.giveAccess();
        waitAccessEnd(packAccess);
    }

    /**
     * Metodo para tratar de uma acao de avanco de tela.
     */
    private void trataActionNext() {
        PackAccess packAccess = new PackAccess(this);

        if (isPreFinish) {
            packAccess.addTalkAndVibrateAccess(
                    MainActivity.getProperties().getProperty("prefinish_next")
                            + " " + mark.getNome(),
                    Vibrate.Level.DEFAULT
            );
            isPreFinish = false;
        }

        if (verificaSelected(selectedMenu)) {
            String selecionado = lista.get(selectedMenu);
            boolean nextIsLista = false;

            if (data.pilhaIsEmpty()) {
                lista = data.getJSONFromMark(selectedMenu);
                nextIsLista = true;
            } else if (data.isJSONObject(selecionado)) {
                lista = data.getJSONFromObject(selecionado);
                nextIsLista = true;
            }

            if (nextIsLista) {
                reloadView(lista);
                selectedMenu = -1;

                packAccess.addTalkAndVibrateAccess(
                        MainActivity.getProperties().getProperty("menu_next")
                                + " " + selecionado,
                        Vibrate.Level.DEFAULT
                );
            } else {
                String informacao = data.getInfoFromObject(selecionado);
                packAccess.addTalkAndVibrateAccess(informacao, Vibrate.Level.FAST);
            }
        } else {
            packAccess.addTalkAccess(
                    MainActivity.getProperties().getProperty("none_next")
            );
        }

        packAccess.giveAccess();
        waitAccessEnd(packAccess);
    }

    /**
     * Metodo para tratar de uma acao de menu para cima.
     */
    public void trataActionUp() {
        setSelected(selectedMenu, false);

        if (!verificaSelected(selectedMenu)) {
            selectedMenu = -1;
        }

        selectedMenu = (selectedMenu + 1) % lista.size();
        moveMenuVertical();

        setSelected(selectedMenu, true);
    }

    /**
     * Metodo para tratar de uma acao de menu para baixo.
     */
    public void trataActionDown() {
        setSelected(selectedMenu, false);

        if (!verificaSelected(selectedMenu)) {
            selectedMenu = 0;
        }

        selectedMenu = (selectedMenu == 0) ? (lista.size() - 1) : (selectedMenu - 1);
        moveMenuVertical();

        setSelected(selectedMenu, true);
    }

    /**
     * Verifica se a posicao selecionada e' valida.
     *
     * @param s a posicao selecionada
     * @return <b>TRUE</b>, se a posicao e' valida
     */
    private boolean verificaSelected(int s) {
        return s >= 0 && s < lista.size();
    }

    /**
     * Movimenta o menu na vertical.
     */
    private void moveMenuVertical() {
        if (isPreFinish) {
            isPreFinish = false;
        }

        if (verificaSelected(selectedMenu)) {
            String selecionado = lista.get(selectedMenu);
            PackAccess pa = new PackAccess(this);
            pa.addTalkAndVibrateAccess(selecionado, Vibrate.Level.FAST).giveAccess();
            waitAccessEnd(pa);
        }
    }

    /**
     * Inicia a lista de itens.
     */
    private void reloadView(List<String> lst) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                lst
        );

        ListView view = (ListView) findViewById(R.id.lista);
        view.setAdapter(adapter);
    }

    /**
     * Seleciona ou remove a selecao um item de menu na tela.
     *
     * @param selected o item
     * @param checked  <b>TRUE</b> para selecionar ou <b>FALSE</b> para remover a selecao
     */
    private void setSelected(int selected, boolean checked) {
        if (verificaSelected(selected)) {
            ListView view = (ListView) findViewById(R.id.lista);
            view.setItemChecked(selected, checked);
        }
    }
}
